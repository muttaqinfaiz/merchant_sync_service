package model

import "time"

type Terminal struct {
	TerminalId 		int64  	`json:"terminal_id" gorm:"column:terminal_id;primarykey"` 	// char 1-8
	MerchantId 		int64  	`json:"merchant_id" gorm:"column:merchant_id"` 				// char 9-23
	Status     		string 	`json:"status" gorm:"column:status"`           				// char 24-24
	SerialNumber 	string  `json:"serial_number" gorm:"column:serial_number"` 			// char 9-23
	IMEI 			string  `json:"imei" gorm:"column:imei"` 							// char 9-23
	CreatedAt 		time.Time
	UpdatedAt 		time.Time
}
