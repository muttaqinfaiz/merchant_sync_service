package model

import "time"

type Merchant struct {
	No                       int64  `json:"no" gorm:"column:no;primarykey"`                                                   // char 1-15
	MainNo                   int64  `json:"main_no" gorm:"column:main_no"`                                         // char 16-30
	RegistrationName         string `json:"registration_name" gorm:"column:registration_name"`                     // char 31-60
	MainName                 string `json:"main_registration_name" gorm:"column:main_registration_name"`           // char 61-90
	BusinessContactName      string `json:"business_contact_name" gorm:"column:business_contact_name"`             // char 91-190
	BusinessContactMobileNo  string `json:"business_contact_mobile_no" gorm:"column:business_contact_mobile_no"`   // char 191-210
	BusinessContactEmail     string `json:"business_contact_email" gorm:"column:business_contact_email"`           // char 211-330
	BusinessContactTel       string `json:"business_contact_tel" gorm:"column:business_contact_tel"`               // char 331-350
	TechnicalContactName     string `json:"technical_contact_name" gorm:"column:technical_contact_name"`           // char 351-450
	TechnicalContactMobileNo string `json:"technical_contact_mobile_no" gorm:"column:technical_contact_mobile_no"` // char 451-470
	TechnicalContactEmail    string `json:"technical_contact_email" gorm:"column:technical_contact_email"`         // char 471-590
	TechnicalContactTel      string `json:"technical_contact_tel" gorm:"column:technical_contact_tel"`             // char 591-610
	RegistrationAddress      string `json:"registration_address" gorm:"column:registration_address"`               // char 611-770
	AreaCode                 string `json:"area_code" gorm:"column:area_code"`                                     // char 771-785
	CategoryCode             string `json:"category_code" gorm:"column:category_code"`                             // char 786-789
	CreditLevel              string `json:"credit_level" gorm:"column:credit_level"`                               // char 790-792
	ValidStartDate           string `json:"valid_start_date" gorm:"column:valid_start_date"`                       // char 793-800
	StopTrxDate              string `json:"stop_trx_date" gorm:"column:stop_trx_date"`                             // char 801-808
	IsMonitored              string `json:"is_monitored" gorm:"column:is_monitored"`                               // char 809-809
	AmountType               string `json:"amount_type" gorm:"column:amount_type"`                                 // char 810-819
	IsCreditChecked          string `json:"is_credit_checked" gorm:"column:is_credit_checked"`                     // char 820-820
	BusinessStartTime        string `json:"business_start_time" gorm:"column:business_start_time"`                 // char 821-824
	BusinessEndTime          string `json:"business_end_time" gorm:"column:business_end_time"`                     // char 825-828
	PayCycle                 string `json:"pay_cycle" gorm:"column:pay_cycle"`                                     // char 829-848
	Status                   string `json:"status" gorm:"column:status"`                                           // char 849-849
	NameEn                   string `json:"name_en" gorm:"column:name_en"`                                         // char 850-909
	AddrEn                   string `json:"addr_en" gorm:"column:addr_en"`                                         // char 910-1109
	CityEn                   string `json:"city_en" gorm:"column:city_en"`                                         // char 1110-1129
	ZipCodeEn                string `json:"zip_code_en" gorm:"column:zip_code_en"`                                 // char 1130-1135
	CardType                 string `json:"card_type" gorm:"column:card_type"`                                     // char 1136-1165
    IsEmailSended 			 bool `json:"is_email_sended" gorm:"column:is_email_sended;default:false"`   
	Password				 string `json:"password" gorm:"column:password"`   
	PasswordTemp			 string `json:"password_temp" gorm:"column:password_temp"`   
	CreatedAt time.Time
	UpdatedAt time.Time
}
