package main

import (
	"fmt"
	"log"
	"merchant_sync_service/database"
	"merchant_sync_service/functions"
	"os"
	"path/filepath"
	"time"
	"strconv"

	"github.com/joho/godotenv"
	"gorm.io/gorm"
)

func checkFolder(folderPath string, interval time.Duration, db *gorm.DB) {
	// // Map to store last modified times of files
	// lastModified := make(map[string]time.Time)

	for {
		timestamp := time.Now().Format("2006_01_02_15:04:05") + " |"
		files, err := os.ReadDir(folderPath)
		if err != nil {
			fmt.Println(timestamp,"Error reading directory:", err)
			return
		}

		for _, file := range files {
			fileName := file.Name()
			filePath := filepath.Join(folderPath, fileName)

			// Get file info
			fileInfo, err := file.Info()
			if err != nil {
				fmt.Println(timestamp,"Error getting file info for", fileName, ":", err)
				continue
			}
			if fileInfo.IsDir() {
				continue
			} else {

				// Check if file exists in the map
				// lastModTime, ok := lastModified[filePath]
				// if !ok || fileInfo.ModTime().After(lastModTime) {
				// 	// File is new
				// 	fmt.Println(timestamp,"New file:", filePath)
				// }
				switch fileName {
				case "MERCH.IMP":
					fmt.Println(timestamp,"File name matches MERCH.IMP: ", fileName)
					functions.MerchantImpScanner(filePath, db)
				case "TERMINAL.IMP":
					fmt.Println(timestamp,"File name matches TERMINAL.IMP: ", fileName)
					functions.TerminalImpScanner(filePath, db)
				default:
					fmt.Println(timestamp,"File name does not match: ", fileName)
				}

				// Update last modified time for the file
				// lastModified[filePath] = fileInfo.ModTime()
			}
		}

		// Get the previous day's date
		now := time.Now()
		previousDay := now.AddDate(0, 0, -1)
		previousLogFileName := fmt.Sprintf("app_%04d_%02d_%02d.log", previousDay.Year(), previousDay.Month(), previousDay.Day())
		previousLogFilePath := filepath.Join(os.Getenv("LOG_FOLDER"), previousLogFileName)

		// Check if the previous day's log file exists
		if _, err := os.Stat(previousLogFilePath); os.IsNotExist(err) {
			currentLogFilePath := filepath.Join(os.Getenv("LOG_FOLDER"), "app.log")
			// If the previous day's log file does not exist, rename current log file
			if _, err := os.Stat(currentLogFilePath); err == nil {
				err = os.Rename(currentLogFilePath, previousLogFilePath)
				if err != nil {
					log.Fatalf(timestamp,"Failed to rename log file: %v", err)
				}

				// Create a new empty current log file
				file, err := os.Create(currentLogFilePath)
				if err != nil {
					log.Fatalf(timestamp,"Failed to create new log file: %v", err)
				}
				file.Close()
			}
		}


		// Sleep for the specified interval before checking again
		time.Sleep(interval) // BLOCKING The Code
	}
}

func main() {

	timestamp := time.Now().Format("2006_01_02_15:04:05") + " |"
	// Load environment variables from .env file
	err := godotenv.Load()
	if err != nil {
		fmt.Println(timestamp,"Error loading .env file:", err)
		return
	}

	// Get folder path from environment variable
	folderPath := os.Getenv("FOLDER_PATH")
	if folderPath == "" {
		fmt.Println(timestamp,"FOLDER_PATH is not set in .env file")
		return
	}

	dbUser := os.Getenv("DB_MERCHANT_USER")
	dbPass := os.Getenv("DB_MERCHANT_PASSWORD")
	dbHost := os.Getenv("DB_MERCHANT_HOST")
	dbPort := os.Getenv("DB_MERCHANT_PORT")
	dbName := os.Getenv("DB_MERCHANT_NAME")
	logFolder := os.Getenv("LOG_FOLDER")

	scanIntervalStr := os.Getenv("FOLDER_SCAN_INTERVAL_S")

	logFilePath := filepath.Join(logFolder, "app.log")

    // Create log directory if it doesn't exist
    errFolder := os.MkdirAll(logFolder, os.ModePerm)
    if errFolder != nil {
        log.Fatalf(timestamp,"Failed to create log directory: %v", errFolder)
    }

    // Open log file
    logFile, err := os.OpenFile(logFilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
    if err != nil {
        log.Fatalf(timestamp,"Failed to open log file: %v", err)
    }
    defer logFile.Close()

    // Set output to log file
    log.SetOutput(logFile)
	
	// Redirect stdout and stderr to log file
	os.Stdout = logFile
	os.Stderr = logFile

	//PREPARE DB
	dbURI := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", dbUser, dbPass, dbHost, dbPort, dbName)
	db, err := database.InitMerchantDB(dbURI)
	if err != nil {
		fmt.Println(timestamp,"Failed to connect to database: %v", err)
	}
	fmt.Println(timestamp,"DB CONNECTED")

	// Check if the environment variable is set
    if scanIntervalStr == "" {
        fmt.Println(timestamp,"FOLDER_SCAN_INTERVAL_S is not set")
        return
    }

    // Convert the string to an integer
    scanInterval, errInterval := strconv.Atoi(scanIntervalStr)
    if errInterval != nil {
        fmt.Printf(timestamp,"Error converting FOLDER_SCAN_INTERVAL_S to int: %v\n", errInterval)
        return
    }
	interval := time.Duration(scanInterval) * time.Second // Adjust the interval as needed

	checkFolder(folderPath, interval, db)
}
