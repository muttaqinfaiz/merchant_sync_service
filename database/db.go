package database

import (
	"log"

	"merchant_sync_service/model"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// InitDB initializes and returns a database connection
func InitDB(dbURI string) (*gorm.DB, error) {
	db, err := gorm.Open(mysql.Open(dbURI), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	return db, nil
}

func InitMerchantDB(dbURI string) (*gorm.DB, error) {
	db, err := gorm.Open(mysql.Open(dbURI), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	// Run migrations
	if err := db.AutoMigrate(
		&model.Merchant{},
		&model.Terminal{},
	); err != nil {
		log.Fatal(err)
	}

	return db, nil
}
