package functions

import (
	"bufio"
	"fmt"
	"merchant_sync_service/model"
	"os"
	"strconv"
	"strings"

	"gorm.io/gorm"
)

func MerchantImpScanner(filePath string, db *gorm.DB) {
	// Open the file for reading
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer file.Close()

	// Create an array to hold the Merchant structs
	var merchants []model.Merchant

	// Create a scanner to read the file line by line
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()

		// Ignore lines starting with 'T' or empty lines
		if strings.HasPrefix(line, "T") || len(line) == 0 {
			continue
		}

		// Check if the line length is greater than or equal to 1165 (to avoid index out of range)
		if len(line) >= 1165 {
			no, err := strconv.ParseInt(strings.TrimSpace(line[0:15]), 10, 64)
			if err != nil {
				fmt.Println("Error parsing int64:", err)
			}
			main_no, err := strconv.ParseInt(strings.TrimSpace(line[15:30]), 10, 64)
			if err != nil {
				fmt.Println("Error parsing int64:", err)
			}
			// password_temp := GenerateRandomCapitalText(12)
			// password := HashPassword(password_temp)
			
			// Password:                 password,
			// PasswordTemp:             password_temp,

			merchant := model.Merchant{
				No:                       no,
				MainNo:                   main_no,
				RegistrationName:         strings.TrimSpace(line[30:60]),
				MainName:                 strings.TrimSpace(line[60:90]),
				BusinessContactName:      strings.TrimSpace(line[90:190]),
				BusinessContactMobileNo:  strings.TrimSpace(line[190:210]),
				BusinessContactEmail:     strings.TrimSpace(line[210:330]),
				BusinessContactTel:       strings.TrimSpace(line[330:350]),
				TechnicalContactName:     strings.TrimSpace(line[350:450]),
				TechnicalContactMobileNo: strings.TrimSpace(line[450:470]),
				TechnicalContactEmail:    strings.TrimSpace(line[470:590]),
				TechnicalContactTel:      strings.TrimSpace(line[590:610]),
				RegistrationAddress:      strings.TrimSpace(line[610:770]),
				AreaCode:                 strings.TrimSpace(line[770:785]),
				CategoryCode:             strings.TrimSpace(line[785:789]),
				CreditLevel:              strings.TrimSpace(line[789:792]),
				ValidStartDate:           strings.TrimSpace(line[792:800]),
				StopTrxDate:              strings.TrimSpace(line[800:808]),
				IsMonitored:              strings.TrimSpace(line[808:809]),
				AmountType:               strings.TrimSpace(line[809:819]),
				IsCreditChecked:          strings.TrimSpace(line[819:820]),
				BusinessStartTime:        strings.TrimSpace(line[820:824]),
				BusinessEndTime:          strings.TrimSpace(line[824:828]),
				PayCycle:                 strings.TrimSpace(line[828:848]),
				Status:                   strings.TrimSpace(line[848:849]),
				NameEn:                   strings.TrimSpace(line[849:909]),
				AddrEn:                   strings.TrimSpace(line[909:1109]),
				CityEn:                   strings.TrimSpace(line[1109:1129]),
				ZipCodeEn:                strings.TrimSpace(line[1129:1135]),
				CardType:                 strings.TrimSpace(line[1135:1165]),
			}

			// Append the merchant to the merchants array
			merchants = append(merchants, merchant)
		}
	}

	// Check for scanner errors
	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading file:", err)
		return
	}

	result := db.Save(&merchants)
	if result.Error != nil {
		fmt.Println("Error inserting batch into database: %v", result.Error)
	}
	// Print the parsed merchants
	// for _, m := range merchants {
	// 	fmt.Println(m)
	// }
	err = os.Remove(filePath)
	if err != nil {
		fmt.Println("Error deleting file:", err)
		return
	}
}

func TerminalImpScanner(filePath string, db *gorm.DB) {
	// Open the file for reading
	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer file.Close()

	// Create an array to hold the Merchant structs
	var terminals []model.Terminal

	// Create a scanner to read the file line by line
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()

		// Ignore lines starting with 'T' or empty lines
		if strings.HasPrefix(line, "T") || len(line) == 0 {
			continue
		}

		// Check if the line length is greater than or equal to 1165 (to avoid index out of range)
		if len(line) >= 24 {
			terminal_id, err := strconv.ParseInt(strings.TrimSpace(line[0:8]), 10, 64)
			if err != nil {
				fmt.Println("Error parsing terminal_id int64:", err)
			}
			merchant_id, err := strconv.ParseInt(strings.TrimSpace(line[8:23]), 10, 64)
			if err != nil {
				fmt.Println("Error parsing merchant_id int64:", err)
			}
			terminal := model.Terminal{
				TerminalId: terminal_id,
				MerchantId: merchant_id,
				Status:     strings.TrimSpace(line[23:24]),
			}

			// Append the merchant to the merchants array
			terminals = append(terminals, terminal)
		}
	}

	// Check for scanner errors
	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading file:", err)
		return
	}

	if len(terminals) > 0 {
		result := db.Save(&terminals)
		if result.Error != nil {
			fmt.Println("Error inserting batch into database: %v", result.Error)
		}
	}
	// Print the parsed merchants
	// for _, m := range terminals {
	// 	fmt.Println(m)
	// }
	err = os.Remove(filePath)
	if err != nil {
		fmt.Println("Error deleting file:", err)
		return
	}
}
