
package functions

import (
	"math/rand"
	"time"
)

func GenerateRandomText(charNum int) string {
	// Define the character set from which to generate the token
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	// Seed the random number generator with the current time
	rand.Seed(time.Now().UnixNano())

	// Create a buffer to store the generated characters
	token := make([]byte, charNum)

	// Generate random characters for the token
	for i := range token {
		token[i] = charset[rand.Intn(len(charset))]
	}

	return string(token)
}
func GenerateRandomCapitalText(charNum int) string {
	// Define the character set from which to generate the token
	const charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	// Seed the random number generator with the current time
	rand.Seed(time.Now().UnixNano())

	// Create a buffer to store the generated characters
	token := make([]byte, charNum)

	// Generate random characters for the token
	for i := range token {
		token[i] = charset[rand.Intn(len(charset))]
	}

	return string(token)
}